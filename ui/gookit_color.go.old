package ui

import (
	"fmt"
	"github.com/gookit/color"
	"io"
	"os"
	"strings"
)

type GookitColorPrinter struct {
	Primary   color.Color
	Secondary color.Color
	Bg        color.Color
}

func NewGookitColorPrinter(primary, secondary, bg color.Color) *GookitColorPrinter {
	tmp := GookitColorPrinter{
		Primary:   primary,
		Secondary: secondary,
		Bg:        bg,
	}
	return &tmp
}

func (g *GookitColorPrinter) Fprint(w io.Writer, status Status) {
	progress := strings.Repeat("-", status.Progress.N) + strings.Repeat(" ", status.Progress.D-status.Progress.N)
	msg := fmt.Sprintf("[%s] %s %s", progress, status.Icon, status.Message)
	color.Fprintln(w, msg)
}

func (g *GookitColorPrinter) Print(status Status) {
	g.Fprint(os.Stdout, status)
}
