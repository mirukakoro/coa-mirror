package compiler

import (
	"github.com/alecthomas/repr"
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/signals"
	"gitlab.com/coalang/go-compiler/types"
	"gitlab.com/coalang/go-compiler/ui"
)

type Call struct {
	Ctx *types.SrcContext `json:"c"`

	Callee Obj  `json:"a"`
	Args   *Map `json:"r"`
}

func (c *Call) From(thing parser.Thing) Obj {
	panic("implement me")
}

func NewCall(ctx *types.SrcContext, callee Obj, args *Map) *Call {
	tmp := Call{
		Ctx:    ctx,
		Callee: callee,
		Args:   args,
	}

	return &tmp
}

func (c *Call) Code() string {
	return c.Callee.Code() + (*c.Args).Code()
}

func (c *Call) SrcCtx() *types.SrcContext {
	return c.Ctx
}

func (c *Call) Eval(ctx *Context) (Obj, error) {
	isDef := false
	var ref *Ref
	if ref2, ok := c.Callee.(*Ref); ok {
		isDef = ref2.IsDef()
		ref = ref2
	}
	if isDef {
		ref.Ref = ref.CutSpecial()
	}
	ui.Debugf("call:%v,%s", isDef, repr.String(ref))
	args := NewMap(
		nil,
		map[string]Obj{},
	)

	for i, arg := range c.Args.Objs {
		re, err := arg.Eval(ctx)
		if err != nil {
			return nil, err
		}

		args.Objs[i] = re
	}

	tmp := ctx.Mem.Merge(args.Generic())

	newCtx := &Context{
		Ctx: c.Ctx,
		Mem: &tmp,
	}
	if isDef && ref != nil {
		args, _ := SplitMap(newCtx.Mem.Objs)
		if len(args) == 0 {
			return nil, signals.Args{
				Ctx:    c.Callee.SrcCtx(),
				Ref:    c.Callee.Code(),
				Wanted: "({ one or more })",
				Got:    repr.String(args),
			}
		}
		ctx.Mem.SetChild(ref.Ref, args[0])
	}
	re, err := c.Callee.Eval(newCtx)
	if isDef {
		if ref == nil {
			return nil, signals.Invalid{
				Ctx: c.Ctx,
				M:   "ref is nil",
			}
		}
		ctx.Mem.SetChild(ref.Ref, re)
		return nil, err
	}

	return re, err
}

func (c *Call) Child(string) Obj {
	return nil
}

func (c *Call) String() string {
	return c.Code()
}
