package compiler

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/signals"
	"gitlab.com/coalang/go-compiler/types"
	"strconv"
)

type Map struct {
	Ctx *types.SrcContext `json:"c"`

	Objs map[string]Obj `json:"o"`
}

func (m *Map) From(thing parser.Thing) Obj {
	panic("implement me")
}

type Mapper interface {
	Mapper() *Generic
}

func NewMap(ctx *types.SrcContext, objs map[string]Obj) *Map {
	tmp := Map{
		Ctx:  ctx,
		Objs: objs,
	}
	return &tmp
}

func (m *Map) SrcCtx() *types.SrcContext {
	return m.Ctx
}

func (m *Map) Eval(context *Context) (Obj, error) {
	args, _ := SplitMap(context.Mem.Objs)
	if len(args) == 0 {
		return nil, signals.Args{
			Ctx:    m.Ctx,
			Ref:    fmt.Sprintf("map %v", m),
			Wanted: "( ..text )",
			Got:    context.Mem.Code(),
		}
	}
	if _, ok := args[0].(*Text); ok { // TODO: change to `switch *.(type) {}`
		return nil, signals.Args{
			Ctx:    m.Ctx,
			Ref:    fmt.Sprintf("map %v", m),
			Wanted: "( ..text )",
			Got:    context.Mem.Code(),
		}
	}
	text := args[0].(*Text)
	if re, ok := m.Objs[text.Text]; ok {
		return re, nil
	} else {
		return nil, signals.Unreal{
			Ctx: m.Ctx,
			Ref: text.Text,
		}
	}
}

func (m *Map) Child(s string) Obj {
	return nil
}

func SplitMap(m map[string]Obj) ([]Obj, map[string]Obj) {
	a := make([]Obj, len(m))
	b := map[string]Obj{}
	for k, v := range m {
		i, err := strconv.ParseInt(k, 10, 64)
		if err == nil {
			a[i] = v
		} else {
			b[k] = v
		}
	}
	return a, b
}

func (m *Map) Generic() Generic {
	return Generic{
		Ctx:  m.Ctx,
		Objs: m.Objs,
	}
}

func (m *Map) Code() string {
	re := "(\n"

	a, b := SplitMap(m.Objs)
	for _, obj := range a {
		if obj == nil {
			continue
		}
		re += IndentStr(obj.Code()+"\n", "  ")
	}
	for key, obj := range b {
		if obj == nil {
			continue
		}
		// Not calling Call.Code since it would call Map.Code, therefore creating a loop
		re += IndentStr(fmt.Sprintf("%s.=(%s)\n", key, obj.Code()), "  ")
	}
	re += "\n)"
	return re
}

func (m *Map) String() string {
	return m.Code()
}
