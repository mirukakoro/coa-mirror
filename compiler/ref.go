package compiler

import (
	"gitlab.com/coalang/go-compiler/parser"
	"gitlab.com/coalang/go-compiler/signals"
	"gitlab.com/coalang/go-compiler/types"
	"gitlab.com/coalang/go-compiler/ui"

	"strings"
)

type Ref struct {
	Ctx *types.SrcContext `json:"c"`

	Ref string `json:"r"`
}

func (r *Ref) From(thing parser.Thing) Obj {
	ref := thing.(parser.Ref)
	return NewRef(
		toSrcCtx(ref.Pos, ref.EndPos, ref.Tokens),
		ref.Ref,
	)
}

func NewRef(ctx *types.SrcContext, ref string) *Ref {
	tmp := Ref{
		Ctx: ctx,
		Ref: ref,
	}

	return &tmp
}

func (r *Ref) SrcCtx() *types.SrcContext {
	return r.Ctx
}

func (r *Ref) Code() string {
	return r.Ref
}

func (r *Ref) IsDef() bool {
	return strings.HasSuffix(r.Ref, "..=")
}

func (r *Ref) CutSpecial() string {
	return r.Ref[:strings.LastIndex(r.Ref, "..")]
}

func (r *Ref) Eval(ctx *Context) (Obj, error) {
	ref := r.Ref
	//isDef := strings.HasSuffix(r.Ref, "..=")
	//
	//if isDef {
	//	ref = r.Ref[:len(r.Ref)-3] // cut off ..=
	//}

	re := ctx.Mem.Child(ref)
	ui.Debugf("ref:%s,%s", ref, re)

	//if isDef {
	//	ui.Debugf("ref:%s,%s,%v", ref, re, isDef)
	//	ctx.Mem.SetChild(ref, reEval)
	//
	//	return reEval, errEval
	//}

	if re == nil {
		err := signals.Unreal{
			Ctx: r.Ctx,
			Ref: ref,
		}

		return nil, err
	}

	return re.Eval(ctx)
}

func (r *Ref) Child(s string) Obj {
	return NewRef(
		nil,
		strings.Join([]string{r.Ref, s}, "."),
	)
}

func (r *Ref) String() string {
	return r.Ref
}
