package compiler

//import (
//	"github.com/alecthomas/participle/v2/lexer"
//
//	"gitlab.com/coalang/go-compiler/parser"
//	"gitlab.com/coalang/go-compiler/types"
//
//	"log"
//	"strconv"
//)
//
//func CompileRawBlock(rawBlock *parser.RawBlock) *Block {
//	re := Block{
//		Ctx:  toSrcCtx(rawBlock.Pos, rawBlock.EndPos, rawBlock.Tokens),
//		Sign: nil,
//		Objs: []Obj{},
//	}
//	for _, obj := range rawBlock.Objs {
//		re.Objs = append(re.Objs, CompileObj(&obj))
//	}
//	return &re
//}
//
//func CompileObj(obj *parser.Obj) Obj {
//	var re Obj
//
//	var prefix, root, suffix = obj.Prefix, obj.Root, obj.Suffix
//
//	var toInt = map[bool]int{
//		true:  1,
//		false: 0,
//	}
//
//	if toInt[root.Map != nil]+toInt[root.Block != nil]+toInt[root.Text != nil]+toInt[root.Ref != nil] > 1 {
//		log.Panicf("obj.Root has more than one non nil pointers: %s", root)
//	}
//
//	switch {
//	case root.Block != nil:
//		// Unsigned Block - [...]
//		re = NewBlock(
//			toSrcCtx(root.Block.Pos, root.Block.EndPos, root.Block.Tokens),
//			nil,
//			CompileBlockToObjs(root.Block),
//		)
//	case root.Block != nil && suffix != nil:
//		// Unsigned Block Eval - [...](...)
//		re = NewCall(
//			nil,
//			NewBlock(
//				toSrcCtx(root.Block.Pos, root.Block.EndPos, root.Block.Tokens),
//				nil,
//				CompileBlockToObjs(root.Block),
//			),
//			NewMap(
//				toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
//				CompileMapToObjs(suffix.Map),
//			),
//		)
//	case prefix != nil && prefix.Map != nil && root.Block != nil:
//		// Signed Block - (...)[...]
//		re = NewBlock(
//			toSrcCtx(root.Pos, root.EndPos, root.Tokens),
//			NewMap(
//				toSrcCtx(prefix.Pos, prefix.EndPos, prefix.Tokens),
//				CompileMapToObjs(prefix.Map),
//			),
//			CompileBlockToObjs(root.Block),
//		)
//		//re = Block{
//		//	Ctx: toSrcCtx(root.Block.Pos, root.Block.EndPos, root.Block.Tokens),
//		//	Sign: &Map{
//		//		Ctx:  toSrcCtx(prefix.Pos, prefix.EndPos, prefix.Tokens),
//		//		Objs: CompileMapToObjs(prefix.Map),
//		//	},
//		//	Objs: CompileBlockToObjs(root.Block),
//		//}
//	case prefix != nil && suffix != nil && prefix.Map != nil && root.Block != nil && suffix.Map != nil:
//		// Signed Block Eval - (...)[...](...)
//		re = NewCall(
//			toSrcCtx(prefix.Pos, prefix.EndPos, prefix.Tokens),
//			NewBlock(
//				toSrcCtx(root.Pos, root.EndPos, root.Tokens),
//				NewMap(
//					toSrcCtx(prefix.Pos, prefix.EndPos, prefix.Tokens),
//					CompileMapToObjs(prefix.Map),
//				),
//				CompileBlockToObjs(root.Block),
//			),
//			NewMap(
//				toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
//				CompileMapToObjs(suffix.Map),
//			),
//		)
//	case suffix == nil && root.Text != nil:
//		// Text - "..."
//		re = NewText(
//			toSrcCtx(root.Text.Pos, root.Text.EndPos, root.Text.Tokens),
//			root.Text.Text,
//		)
//	case suffix == nil && root.Ref != nil:
//		// Ref - ...
//		re = NewRef(
//			toSrcCtx(root.Ref.Pos, root.Ref.EndPos, root.Ref.Tokens),
//			root.Ref.Ref,
//		)
//	case suffix != nil && root.Ref != nil && suffix.Map != nil:
//		// Ref - ...
//		re = NewCall(
//			nil,
//			NewRef(
//				toSrcCtx(root.Ref.Pos, root.Ref.EndPos, root.Ref.Tokens),
//				root.Ref.Ref,
//			),
//			NewMap(
//				toSrcCtx(suffix.Pos, suffix.EndPos, suffix.Tokens),
//				CompileMapToObjs(suffix.Map),
//			),
//		)
//	case suffix == nil && root.Map != nil:
//		// Map - (..)
//		re = NewMap(
//			toSrcCtx(root.Pos, root.EndPos, root.Tokens),
//			CompileMapToObjs(root.Map),
//		)
//	default:
//		log.Panic("obj does not fit any description")
//	}
//	return re
//}
//
//func CompileBlockToObjs(block *parser.Block) []Obj {
//	re := make([]Obj, len(block.Objs))
//	for i, obj := range block.Objs {
//		re[i] = CompileObj(&obj)
//	}
//	return re
//}
//
//func CompileMapToObjs(m *parser.Map) map[string]Obj {
//	re := map[string]Obj{}
//	for i, obj := range m.Objs {
//		re[strconv.FormatInt(int64(i), 10)] = CompileObj(&obj)
//	}
//	return re
//}
