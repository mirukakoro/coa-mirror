package parser

import (
	"bytes"
	"errors"
	"github.com/alecthomas/participle/v2"
	"github.com/alecthomas/participle/v2/lexer/stateful"
	"io"
)

// Parse is a function to parse an io.Reader and convert it into an *RawBlock and an error.
func Parse(filename string, src io.Reader) (*RawBlock, error) {
	lex, err := stateful.NewSimple(
		[]stateful.Rule{
			{`OParen`, `\(`, nil},
			{`CParen`, `\)`, nil},
			{`OBrack`, `\[`, nil},
			{`CBrack`, `\]`, nil},
			{`Dot`, `\.`, nil},
			{`TextIn`, `\'[^']*\'`, nil},
			{`RefIn`, `[^\(\)\[\]\{\}\s\r\n\'\.]`, nil},
			{`Space`, `[\s\r\n]+`, nil},
			{`Comment`, `\{[^\}]\}`, nil},
		},
	)
	//lex, err := stateful.New(stateful.Rules{
	//	"Root": {
	//		{`OParen`, `\(`, nil},
	//		{`CParen`, `\)`, nil},
	//		{`OBrack`, `\[`, nil},
	//		{`CBrack`, `\]`, nil},
	//		{`Dot`, `\.`, nil},
	//		{`TextIn`, `\'[^']*\'`, nil},
	//		{`RefIn`, `[^\(\)\[\]\{\}\s\r\n\'\.]`, nil},
	//		{`Space`, `[\s\r\n]+`, nil},
	//		{`Comment`, `\{[^\}]\}`, nil},
	//	},
	//	//"String": {
	//	//	{"Escaped", `\\.`, nil},
	//	//	{"End", `'`, stateful.Pop()},
	//	//	{"Expr", `"`, stateful.Push("Expr")},
	//	//	{"Char", `[^' \\]+`, nil},
	//	//},
	//	//"Expr": {
	//	//	stateful.Include("Root"),
	//	//	{`whitespace`, `\s+`, nil},
	//	//	{"Ident", `[^\s\r\n\[\]\(\)\{\}]+`, nil},
	//	//	{"End", `\"`, stateful.Pop()},
	//	//},
	//})
	if err != nil {
		return nil, err
	}

	parser, err := participle.Build(
		&RawBlock{},
		participle.Lexer(lex),
		participle.Elide("Comment"),
		//participle.Elide("Comment", "Space"),
		participle.UseLookahead(1),
	)

	if err != nil {
		return nil, err
	}

	ast := &RawBlock{}
	srcBuf := new(bytes.Buffer)
	_, err = srcBuf.ReadFrom(src)

	if err != nil {
		return nil, err
	}
	err = parser.ParseString(filename, srcBuf.String(), ast)

	if err != nil {
		return nil, NewParseErrorFromError(
			err.(participle.Error),
			srcBuf.String(),
		)
		//return nil, err
	}
	if ast == nil {
		return nil, errors.New("ast is nil")
	}

	return ast, nil
}
