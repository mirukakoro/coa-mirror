package signals

import (
	"fmt"
	"gitlab.com/coalang/go-compiler/types"
)

type Invalid struct {
	Ctx *types.SrcContext

	M string
}

func (i Invalid) SrcCtx() *types.SrcContext {
	return i.Ctx
}

func (i Invalid) Msg() string {
	return fmt.Sprintf("ast is invalid: %s", i.M)
}

func (i Invalid) Error() string {
	ctx := ""
	if i.Ctx != nil {
		ctx = i.Ctx.Fmt() + " "
	}
	msg := ""
	if i.Msg() != "" {
		msg = i.Msg()
	}
	return fmt.Sprintf("%s%s", ctx, msg)
}
