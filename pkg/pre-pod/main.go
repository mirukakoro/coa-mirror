package prepod

import "gitlab.com/coalang/go-compiler/compiler"

type PrePod interface {
	Mem() compiler.Map
}
