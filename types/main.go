package types

import (
	"fmt"
)

type SrcContext struct {
	Pos    SrcPos  `json:"p"`
	EndPos SrcPos  `json:"e"`
	Tokens []Token `json:"t"`
}

func (ctx SrcContext) Fmt() string {
	return ctx.Pos.Fmt()
}

type SrcPos struct {
	Name   string `json:"n"`
	Offset int    `json:"o"`
	Line   int    `json:"l"`
	Column int    `json:"c"`
}

func (pos SrcPos) Fmt() string {
	fmted := ""
	if pos.Name != "" {
		fmted += pos.Name + ":"
	}
	fmted += fmt.Sprintf("%v:%v", pos.Line, pos.Column)
	return fmted
}

type Token struct {
	Type  rune   `json:"t"`
	Value string `json:"v"`
	Pos   SrcPos `json:"p"`
}
