module gitlab.com/coalang/go-compiler

go 1.15

require (
	github.com/alecthomas/participle/v2 v2.0.0-alpha3
	github.com/alecthomas/repr v0.0.0-20181024024818-d37bc2a10ba1
	github.com/fatih/color v1.10.0
)
